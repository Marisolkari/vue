import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({

  state: {
    libros: [
      {nombre: 'Novelas policiales', cantidad: 0},
      {nombre: 'Novelas de romance', cantidad: 0},
      {nombre: 'Libros de ciencias', cantidad: 0},
      {nombre: 'Cuentos para niños', cantidad: 0},
      {nombre: 'Historias cortas', cantidad: 0},
      {nombre: 'Fábulas y refranes', cantidad: 0},
      {nombre: 'Novelas de terror', cantidad: 0},
      {nombre: 'Poemas', cantidad: 0},
      {nombre: 'Literatura clásica', cantidad: 0}
    ]
  },
  mutations: {
    aumentar(state,index){
      state.libros[index].cantidad ++
    },
    reiniciar(state){
      state.libros.forEach(elemento => {
        elemento.cantidad = 0
      })
    }
 
  },
  actions: {
  },
  modules: {
  }
})


